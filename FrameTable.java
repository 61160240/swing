
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class FrameTable extends javax.swing.JFrame {

    private int row;
    private int col;
    Player o;
    Player x;
    Table table;
    JButton btnTable[][] = null;

    public FrameTable() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        showStatsOX();
        hideNewGame();
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newGame();
                hideNewGame();
            }
        });
    }


    private void initGame() {
        o = new Player('O');
        x = new Player('X');
        table = new Table(o, x);
    }
    private void hideNewGame(){
        btnNewGame.setVisible(false);
    }
    private void showNewGame(){
        btnNewGame.setVisible(true);
    }


    private void initTable() {
        JButton tables[][] = {{btnBoard1, btnBoard2, btnBoard3},
        {btnBoard4, btnBoard5, btnBoard6},
        {btnBoard7, btnBoard8, btnBoard9}};
        btnTable = tables;
        for (int i = 0; i < btnTable.length; i++) {
            for (int j = 0; j < btnTable[i].length; j++) {
                btnTable[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row = Integer.parseInt(coms[0]);
                        col = Integer.parseInt(coms[1]);
                        if (table.setRowCol(row, col)) {
                            showTable();
                            if (table.checkWin()) {
                                showStatsOX();
                                showWin();
                                return;
                            }
                            switchTurn();
                            
                        }

                    }
                });
            }
        }
    }

    public void showRowCol() {
        txtOutput.setText("Row:" + row + " Col:" + col);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBoard1 = new javax.swing.JButton();
        btnBoard2 = new javax.swing.JButton();
        btnBoard3 = new javax.swing.JButton();
        btnBoard4 = new javax.swing.JButton();
        btnBoard5 = new javax.swing.JButton();
        btnBoard6 = new javax.swing.JButton();
        btnBoard7 = new javax.swing.JButton();
        btnBoard8 = new javax.swing.JButton();
        btnBoard9 = new javax.swing.JButton();
        txtOutput = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtShowStatsO = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtShowStatsX = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnBoard1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard1.setText("-");
        btnBoard1.setActionCommand("1,1");
        btnBoard1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard1.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard2.setText("-");
        btnBoard2.setActionCommand("1,2");
        btnBoard2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard3.setText("-");
        btnBoard3.setActionCommand("1,3");
        btnBoard3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard4.setText("-");
        btnBoard4.setActionCommand("2,1");
        btnBoard4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard5.setText("-");
        btnBoard5.setActionCommand("2,2");
        btnBoard5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard6.setText("-");
        btnBoard6.setActionCommand("2,3");
        btnBoard6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard7.setText("-");
        btnBoard7.setActionCommand("3,1");
        btnBoard7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard8.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard8.setText("-");
        btnBoard8.setActionCommand("3,2");
        btnBoard8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnBoard9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnBoard9.setText("-");
        btnBoard9.setActionCommand("3,3");
        btnBoard9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnBoard9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnBoard9.setPreferredSize(new java.awt.Dimension(80, 80));

        txtOutput.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtOutput.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnNewGame.setText("New game");

        jPanel2.setBackground(new java.awt.Color(220, 255, 251));
        jPanel2.setPreferredSize(new java.awt.Dimension(347, 129));

        txtShowStatsO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtShowStatsO.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtShowStatsO, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtShowStatsO, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(220, 255, 251));
        jPanel3.setPreferredSize(new java.awt.Dimension(347, 129));

        txtShowStatsX.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtShowStatsX.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtShowStatsX, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtShowStatsX, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBoard4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBoard5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBoard6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnBoard1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBoard2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBoard3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBoard7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBoard8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBoard9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBoard1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBoard4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnBoard7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBoard9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addComponent(txtOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrameTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrameTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrameTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrameTable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameTable().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBoard1;
    private javax.swing.JButton btnBoard2;
    private javax.swing.JButton btnBoard3;
    private javax.swing.JButton btnBoard4;
    private javax.swing.JButton btnBoard5;
    private javax.swing.JButton btnBoard6;
    private javax.swing.JButton btnBoard7;
    private javax.swing.JButton btnBoard8;
    private javax.swing.JButton btnBoard9;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel txtOutput;
    private javax.swing.JLabel txtShowStatsO;
    private javax.swing.JLabel txtShowStatsX;
    // End of variables declaration//GEN-END:variables

    private void showTable() {
        char data[][] = table.getData();
        for (int i = 0; i < btnTable.length; i++) {
            for (int j = 0; j < btnTable[i].length; j++) {
                btnTable[i][j].setText("" + data[i][j]);
            }
        }
    }

    public void switchTurn() {
        table.switchPlayer();
        showTurn();
    }

    private void showTurn() {
        txtOutput.setText("Turn " + table.getCurrentPlayer().getName());
    }

    public void showWin() {
        if (table.getWinner() == null) {
            txtOutput.setText("Draw !!");
        } else {
            txtOutput.setText(table.getWinner().getName() + " Win !!");
        }
        for (int i = 0; i < btnTable.length; i++) {
            for (int j = 0; j < btnTable[i].length; j++) {
                btnTable[i][j].setEnabled(false);
            }
        }showNewGame();
    }
    private void newGame(){
        table = new Table(o,x);
        showTable();
        for (int i = 0; i < btnTable.length; i++) {
            for (int j = 0; j < btnTable[i].length; j++) {
                btnTable[i][j].setEnabled(true);
            }
        }
    }
    private void showStatsOX() {
        txtShowStatsX.setText("Player X : Win " + x.getWin() + ", Lose " + x.getLose() + ", Draw " + x.getDraw());
        txtShowStatsO.setText("Player O : Win " + o.getWin() + ", Lose " + o.getLose() + ", Draw " + o.getDraw());
        
    }


}
